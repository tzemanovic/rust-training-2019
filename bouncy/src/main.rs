mod parse_args;
use self::parse_args::Frame;
use std::fmt::{Display, Formatter};

struct Ball {
    x: u32,
    y: u32,
    vert: Vert,
    hori: Hori,
}

enum Vert {
    Up,
    Down,
}

enum Hori {
    Left,
    Right,
}

struct Game {
    frame: Frame,
    ball: Ball,
}

impl Game {
    fn new(frame: Frame) -> Game {
        Game {
            frame: frame,
            ball: Ball {
                x: 2,
                y: 4,
                vert: Vert::Down,
                hori: Hori::Right,
            },
        }
    }

    fn step(&mut self) {
        self.ball.bounce(&self.frame);
        self.ball.mv();
    }
}

impl Display for Game {
    fn fmt(&self, fmt: &mut Formatter) -> std::fmt::Result {
        let top_bottom = |fmt: &mut Formatter| {
            write!(fmt, "+")?;
            for _ in 0..self.frame.width {
                write!(fmt, "-")?;
            }
            write!(fmt, "+\n")
        };

        top_bottom(fmt)?;
        for row in 0..self.frame.height {
            write!(fmt, "|")?;
            for column in 0..self.frame.width {
                let c = if row == self.ball.y && column == self.ball.x {
                    'o'
                } else {
                    ' '
                };
                write!(fmt, "{}", c)?;
            }
            write!(fmt, "|\n")?;
        }
        top_bottom(fmt)
    }
}

impl Ball {
    fn bounce(&mut self, frame: &Frame) {
        if self.x == 0 {
            self.hori = Hori::Right
        } else if self.x == frame.width - 1 {
            self.hori = Hori::Left
        }

        if self.y == 0 {
            self.vert = Vert::Down
        } else if self.y == frame.height - 1 {
            self.vert = Vert::Up
        }
    }
    fn mv(&mut self) {
        match self.hori {
            Hori::Left => self.x -= 1,
            Hori::Right => self.x += 1,
        }
        match self.vert {
            Vert::Up => self.y -= 1,
            Vert::Down => self.y += 1,
        }
    }
}

fn main() -> Result<(), self::parse_args::ParseError> {
    let frame = parse_args::parse_args()?;
    let mut game = Game::new(frame);
    let sleep_duration = std::time::Duration::from_millis(33);
    loop {
        println!("{}", game);
        game.step();
        std::thread::sleep(sleep_duration);
    }
}
