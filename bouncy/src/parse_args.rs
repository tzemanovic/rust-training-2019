#[derive(Debug)]
pub struct Frame {
    pub width: u32,
    pub height: u32,
}

#[derive(Debug)]
pub enum ParseError {
    TooFewArgs,
    TooManyArgs,
    InvalidInteger(String),
    FrameTooSmall,
}

const MIN_SIZE: u32 = 5;

struct ParseArgs(std::env::Args);

impl ParseArgs {
    fn new() -> ParseArgs {
        ParseArgs(std::env::args())
    }

    fn require_arg(&mut self) -> Result<String, ParseError> {
        self.0.next().ok_or(ParseError::TooFewArgs)
    }

    fn require_no_args(&mut self) -> Result<(), ParseError> {
        self.0
            .next()
            .map_or(Ok(()), |_| Err(ParseError::TooManyArgs))
    }
}

pub fn parse_args() -> Result<Frame, ParseError> {
    let mut args = ParseArgs::new();

    // skip the command name
    args.require_arg()?;

    let width_str = args.require_arg()?;
    let height_str = args.require_arg()?;
    args.require_no_args()?;
    let width = parse_u32(width_str)?;
    let height = parse_u32(height_str)?;

    if width < MIN_SIZE || height < MIN_SIZE {
        return Err(ParseError::FrameTooSmall);
    }
    Ok(Frame { width, height })
}

fn parse_u32(s: String) -> Result<u32, ParseError> {
    s.parse().or_else(|_| Err(ParseError::InvalidInteger(s)))
}
