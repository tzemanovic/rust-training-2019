extern crate gtk;

use gtk::prelude::*;
use gtk::{Button, Window, WindowType};
use std::cell::RefCell;

fn main() -> Result<(), Box<std::error::Error>> {
    gtk::init()?;
    let window = Window::new(WindowType::Toplevel);
    window.set_title("First GTK+ Program");
    window.set_default_size(350, 70);
    let button = Button::new_with_label("Click me!");
    window.add(&button);
    window.show_all();

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    let file = std::fs::File::create("mylog.txt")?;
    let file = RefCell::new(file);
    button.connect_clicked(move |_| {
        println!("Clicked!");
        use std::io::Write;
        // exercise 6
        // let mut file = std::fs::File::create("mylog.txt").unwrap();
        // file.write_all(b"I was clicked.\n");
        // exercise 7
        file.borrow_mut()
            .write_all(b"I was clicked.\n")
            .unwrap_or_else(|e| eprintln!("Error writing to mylog.txt file {:}", e))
    });

    Ok(gtk::main())
}
