use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn init_guessing() -> (u16, u32) {
    let guess_count: u16 = 0;
    let secret_number = rand::thread_rng().gen_range(1, 101);
    (guess_count, secret_number)
}

fn main() {
    let (mut guess_count, secret_number) = init_guessing();

    println!("Guess the number!");

    loop {
        println!("Please input your guess.");
        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => {
                guess_count = guess_count + 1;
                num
            }
            Err(err) => {
                eprintln!("Input parsing error: {}", err);
                continue;
            }
        };

        println!(
            "You guessed {:?}. You had {:?} guesses.",
            guess, guess_count
        );

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
