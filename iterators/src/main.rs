struct TheAnswer;

impl Iterator for TheAnswer {
    type Item = u32;
    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        Some(42)
    }
}

struct Fib(u64, u64);

fn fib() -> Fib {
    Fib(0, 1)
}

impl Iterator for Fib {
    type Item = u64;
    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        let res = self.0;
        match self.1.checked_add(res) {
            None => None,
            Some(n) => {
                self.0 = self.1;
                self.1 = n;
                Some(res)
            }
        }
    }
}

struct Doubler<I: Iterator> {
    iter: I,
}

impl<I> Iterator for Doubler<I>
where
    I: Iterator,
    I::Item: std::ops::Add<Output = I::Item> + Copy,
{
    type Item = I::Item;
    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        self.iter.next().map(|x| x + x)
    }
}

fn sum<I>(i: I) -> I::Item
where
    I: Iterator,
    I::Item: From<u8> + std::ops::Add<Output = I::Item>,
{
    i.fold(From::from(0u8), std::ops::Add::add)
}
fn main() {
    // 2
    for i in TheAnswer.take(10) {
        println!("The answer to life, the universe, and everything is {}", i);
    }

    // 3
    for (i, fib) in fib().take(47).enumerate() {
        println!("fib {} is {}", i, fib);
    }

    // 4
    let orig_iter = 1..11; // array indices start at 1
    let doubled_iter = Doubler { iter: orig_iter };
    for i in doubled_iter {
        println!("{}", i);
    }
    // 5
    let my_vec: Vec<u32> = (1..11).collect();
    // let my_vec_sum = (1..11).fold(0, std::ops::Add::add);
    let my_vec_sum = sum(1..11);
    println!("{:?}, sum: {}", my_vec, my_vec_sum);
}
