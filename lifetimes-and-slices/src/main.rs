#[derive(Debug)]
struct Person {
    name: Option<String>,
    age: Option<u32>,
}

fn print_person(mut person: Person) {
    match person.name {
        Some(ref name) => println!("Name is {}", name),
        None => println!("No name provided"),
    }

    match person.age {
        // exercise 1
        Some(ref mut age) => {
            println!("Age is {}", age);
            *age += 1;
        }
        None => println!("No age provided"),
    }
    println!("Full Person value: {:?}", person);
}

struct Single<T> {
    next: Option<T>,
}

impl<T> Iterator for Single<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        // exercise 2
        // let mut res = None;
        // std::mem::swap(&mut res, &mut self.next);
        // res

        self.next.take()
    }
}

fn single<T>(t: T) -> Single<T> {
    Single { next: Some(t) }
}

fn foo(name: &String) -> &String {
    let msg = String::from("This is the message");
    message_and_return(&msg, &name)
}

// exercise 3
fn message_and_return<'a>(msg: &String, ret: &'a String) -> &'a String {
    println!("Printing the message: {}", msg);
    ret
}

fn main() {
    // exercise 1
    print_person(Person {
        name: Some(String::from("Alice")),
        age: Some(30),
    });
    // exercise 2
    let actual: Vec<u32> = single(42).collect();
    assert_eq!(vec![42], actual);
    // exercise 3
    let name = String::from("Alice");
    let ret = foo(&name);
    println!("Return value: {}", ret);
    // exercise 4
    let bytearray1: &'static [u8; 22] = b"Hello World in binary!";
    let bytearray2: &'static [u8] = b"Hello World in binary!";
    println!("{:?}", bytearray1);
    println!("{:?}", bytearray2);
    // exercise 5
    for arg in std::env::args() {
        let chars = arg.chars().count();
        let bytes = arg.bytes().count();
        println!("arg: {:}, characters: {:}, bytes: {:}", arg, chars, bytes);
    }
}
