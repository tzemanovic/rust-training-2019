// #[derive(Debug)]
// struct Foobar(i32);

// fn main() {
//     println!("Hello, world!");
//     let x = Foobar(1);
//     println!("{:?}", x);
//     drop(x);
//     // println!("{:?}", x);
// }

// fn drop<T>(_: T) {}

// impl Drop for Foobar {
//     fn drop(&mut self) {
//         println!("Dropping a Foobar: {:?}", self);
//     }
// }

// impl Foobar {
//     fn uses_it(&self) {
//         println!("I consumed a Foobar: {:?}", self);
//     }
// }

// fn uses_foobar(foobar: &Foobar) {
//     println!("I consumed a Foobar: {:?}", foobar);
// }

// fn main() {
//     let x = Foobar(1);
//     println!("Before uses_foobar");
//     uses_foobar(&x);
//     x.uses_it();
//     println!("After uses_foobar");
// }

#[derive(Debug)]
struct Foobar(i32);

fn main() {
    let x: Foobar = Foobar(1);
    let y: Foobar = double(x);
    println!("{}", y.0);
}

fn double(foo: Foobar) -> Foobar {
    Foobar(foo.0 * 2)
}
