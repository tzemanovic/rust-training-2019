fn main() {
    // exercise 1
    let mut x = 5;
    double(&mut x);
    println!("{}", x);
    // exercise 2
    let mut count = 0;
    for _ in InfiniteUnit {
        count += 1;
        println!("count == {}", count);
        if count >= 5 {
            break;
        }
    }
    // exercise 3
    let msg: &str = "Hi!";
    let say_hi = |msg| println!("{}", msg);
    say_hi(msg);
    say_hi(msg);
    // exercise 4
    call_with_hi(say_message);
    call_with_hi(say_message);
    // exercise 5
    let nums: Vec<u32> = (1..11).collect();

    for _ in 1..3 {
        for i in nums.iter().map(|x| x * 2) {
            println!("{}", i);
        }
    }
}
fn double(x: &mut u32) {
    *x *= 2;
}
struct InfiniteUnit;
impl IntoIterator for InfiniteUnit {
    type Item = ();
    // error[E0277]: the size for values of type `(dyn std::iter::Iterator<Item=()> + 'static)` cannot be known at compilation time
    // type IntoIter = Iterator<Item = Self::Item>;
    type IntoIter = std::iter::Repeat<Self::Item>;
    fn into_iter(self) -> Self::IntoIter {
        std::iter::repeat(())
    }
}

fn call_with_hi<F>(f: F)
where
    F: Fn(&str),
{
    f("Hi!");
}

fn say_message(msg: &str) {
    println!("{}", msg)
}
